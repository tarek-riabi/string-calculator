package com.lacombe.kata;

import java.io.Serial;

public class InvalidInputException extends RuntimeException {

	@Serial
	private static final long serialVersionUID = 9202580460835464801L;

	private final String input;

	public InvalidInputException(String input) {
		this.input = input;
	}

	@Override
	public String getMessage() {
		return String.format("Invalid input received: %s", this.input);
	}
}
