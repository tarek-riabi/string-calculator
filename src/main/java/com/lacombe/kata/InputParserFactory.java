package com.lacombe.kata;

import java.util.regex.Pattern;

public class InputParserFactory {
	
	private static final String DEFAULT_STRUCTURE_REGEX = "^((-?[0-9])+[,\n])*-?[0-9]$";
	private static final String CUSTOM_DELIMITER_REGEX = "^//([^0-9])\n(((-?[0-9])+\\1|\n)*-?[0-9])?$";
	private static final Pattern DEFAULT_PATTERN = Pattern.compile(DEFAULT_STRUCTURE_REGEX, Pattern.MULTILINE);
	private static final Pattern CUSTOM_DELIMITER_PATTERN = Pattern.compile(CUSTOM_DELIMITER_REGEX, Pattern.MULTILINE);

	public static InputParser getParser(String numbers) {
		if (numbers != null) {
			if (numbers.isEmpty() || matchInput(DEFAULT_PATTERN, numbers)) {
				return new DefaultInputParser();
			}
			if (matchInput(CUSTOM_DELIMITER_PATTERN, numbers)) {
				return new CustomDelimitedInputParser();
			}
		}
		throw new InvalidInputException(numbers);
	}

	private static boolean matchInput(Pattern pattern, String input) {
		return pattern.matcher(input).lookingAt();
	}
}
