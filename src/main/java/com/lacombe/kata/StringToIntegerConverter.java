package com.lacombe.kata;

public class StringToIntegerConverter {
	
	private static final int DEFAULT_EMPTY_RESULT = 0;
	
	public static Integer convert(String number) {
		if (number.isEmpty()) {
			return DEFAULT_EMPTY_RESULT;
		}
		return Integer.valueOf(number);
	}

}
