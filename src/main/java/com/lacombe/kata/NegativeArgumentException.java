package com.lacombe.kata;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NegativeArgumentException extends RuntimeException {
	
	@Serial
	private static final long serialVersionUID = 7771682193674371784L;
	
	private final List<Integer> negativeArguments = new ArrayList<>();

	public NegativeArgumentException(List<Integer> negatives) {
		super();
		this.negativeArguments.addAll(negatives);
	}

	@Override
	public String getMessage() {
		String argsList = this.negativeArguments
				.stream()
				.map(Object::toString)
				.collect(Collectors.joining(", "));
		return String.format("Negatives not allowed: %s", argsList);
	}
}
