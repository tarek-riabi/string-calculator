package com.lacombe.kata;

import java.util.Arrays;
import java.util.List;

interface InputParser {
	
	default List<Integer> buildArgumentsArray(String numbers) {
		String input = extractArgumentsPartFromInput(numbers);
		String splitterRegex = buildSplittingRegex(numbers);
		return Arrays.stream(splitInput(input, splitterRegex))
				.map(StringToIntegerConverter::convert)
				.toList();
	}

	String buildSplittingRegex(String delimiter);

	String[] splitInput(String input, String splitterRegex);

	String extractArgumentsPartFromInput(String numbers);
}
