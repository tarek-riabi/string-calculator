package com.lacombe.kata;

import java.util.List;

public class StringCalculator {

	public Integer add(String input) {
		InputParser parser = InputParserFactory.getParser(input);
		List<Integer> numbers = parser.buildArgumentsArray(input);
		List<Integer> negatives = numbers.stream().filter(number -> number < 0).toList();
		if (!negatives.isEmpty()) {
			throw new NegativeArgumentException(negatives);
		}
		return computeSum(numbers);
	}

	private Integer computeSum(List<Integer> arguments) {
		return arguments
		.stream()
		.mapToInt(Integer::intValue)
		.sum();
	}
}
