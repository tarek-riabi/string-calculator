package com.lacombe.kata;

public class DefaultInputParser implements InputParser {
	
	private static final String DEFAULT_DELIMITER = ",";
	public static final String ALTERNATIVE_DELIMITER = "\n";
	
	public String extractArgumentsPartFromInput(String numbers) {		
		return numbers;
	}
	
	public String[] splitInput(String input, String regex) {
		return input.split(regex);
	}	

	@Override
	public String buildSplittingRegex(String numbers) {		
		return "[" + DEFAULT_DELIMITER + ALTERNATIVE_DELIMITER + "]";
	}

}