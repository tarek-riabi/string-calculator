package com.lacombe.kata;

public class CustomDelimitedInputParser implements InputParser {
	
	private static final int PREFIX_LENGTH = 4;
	private static final int DELIMITER_INDEX = 2;
	
	private String findDelimiter(String numbers) {
		return numbers.substring(DELIMITER_INDEX, 1 + DELIMITER_INDEX);		
	}
	
	@Override
	public String extractArgumentsPartFromInput(String numbers) {		
		return numbers.substring(PREFIX_LENGTH);		
	}

	@Override
	public String buildSplittingRegex(String numbers) {
		return "[" + findDelimiter(numbers) + DefaultInputParser.ALTERNATIVE_DELIMITER + "]";
	}

	@Override
	public String[] splitInput(String input, String splitterRegex) {		
		return input.split(splitterRegex);
	}
	
	
}
