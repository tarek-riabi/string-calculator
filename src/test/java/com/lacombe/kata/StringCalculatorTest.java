package com.lacombe.kata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StringCalculatorTest {
	
	private StringCalculator calculator;
	
	@BeforeEach
	public void init() {
		calculator = new StringCalculator();
	}
	
	@Test
	public void should_return_zero_when_argument_is_empty_string() {
		assertEquals(0, calculator.add(""));
	}
	
	@Test
	public void should_return_same_number_when_given_a_single_number() {
		assertEquals(5, calculator.add("5"));
	}
	
	@Test
	public void should_return_sum_of_two_numbers_when_given_two_comma_delimited_numbers() {
		assertEquals(3, calculator.add("1,2"));
	}
	
	@Test
	public void should_return_sum_of_all_numbers_when_given_more_than_two_numbers() {
		assertEquals(6, calculator.add("1,2,3"));
	}
	
	@Test
	public void should_take_newline_as_delimiter_instead_of_comma() {
		assertEquals(6, calculator.add("1\n2,3"));
	}
	
	@Test
	public void should_accept_another_delimiter_with_empty_string() {
		assertEquals(0, calculator.add("//;\n"));
	}
	
	@Test
	public void should_accept_another_delimiter_with_one_argument() {
		assertEquals(1, calculator.add("//;\n1"));
	}
	
	@Test
	public void should_accept_another_delimiter_with_two_arguments() {
		assertEquals(3, calculator.add("//;\n1;2"));		
	}
	
	@Test
	public void should_accept_another_delimiter_with_any_number_of_arguments() {
		assertEquals(6, calculator.add("//;\n1;2;3"));
	}
	
	@Test
	public void should_accept_another_delimiter_with_newline() {
		assertEquals(3, calculator.add("//;\n1\n2"));
	}
	
	@Test
	public void should_mix_custom_delimiter_with_newline() {
		assertEquals(6, calculator.add("//;\n1\n2;3"));
	}
	
	@Test
	public void should_mix_custom_delimiter_with_newline_as_last() {
		assertEquals(6, calculator.add("//;\n1;2\n3"));
	}
	
	@Test
	public void should_throw_exception_when_passed_one_negative_argument() {
		assertThrows(NegativeArgumentException.class, () -> calculator.add("-1"));
	}
	
	 @Test
	 public void should_show_the_negative_argument_in_exception_message() {
		 NegativeArgumentException exception = assertThrows(NegativeArgumentException.class, () -> calculator.add("-1"));
		 assertEquals("Negatives not allowed: -1", exception.getMessage());
	 }
	 
	 @Test
	 public void should_show_all_negative_arguments_in_exception_message() {
		 NegativeArgumentException exception = assertThrows(NegativeArgumentException.class, () -> calculator.add("-1,-2"));
		 assertEquals("Negatives not allowed: -1, -2", exception.getMessage());		
	 }
	 
	 @Test
	 public void should_throw_exception_when_given_a_random_string() {
		 assertThrows(InvalidInputException.class, () -> calculator.add("Hello"));
	 }
	 
	 @Test
	 public void should_throw_exception_when_given_two_slashes() {
		 assertThrows(InvalidInputException.class, () -> calculator.add("//"));
	 }
	 
	 @Test
	 public void should_return_zero_when_custom_delimiter_is_present_without_input() {
		 assertEquals(0, calculator.add("//%\n"));
	 }
	 
	 @Test
	 public void should_return_sum_when_custom_delimiter_is_present_but_not_used() {
		 assertEquals(19, calculator.add("//%\n9\n10"));
	 }
	 
	 @Test
	 public void should_throw_exception_when_custom_delimiter_is_present_but_input_uses_default() {
		 assertThrows(InvalidInputException.class, () -> calculator.add("//%\n9,10"));
	 }

	@Test
	public void should_throw_exception_for_null_input() {
		assertThrows(InvalidInputException.class, () -> calculator.add(null));
	}
	 
	 
}
